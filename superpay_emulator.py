from flask import Flask, jsonify, request
import uuid
from json import loads
import logging
import requests as r
from threading import Thread
from time import sleep


logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)

example_com_addr = 'http://0.0.0.0'

TRANSACTIONS = dict()


def transaction_complete(transaction_id):
    sleep(3)
    url = f'{example_com_addr}/transaction/{transaction_id}'
    body = {
        'status': 0
    }

    r.put(url=url, data=body)


@app.route('/transaction', methods={'POST'})
def new_transaction():

    data = loads(request.data.decode('utf-8'))

    logging.info(f'data: {data}')

    error_resp = dict(
        id='',
        error_code=1
    )

    error_resp = jsonify(error_resp)

    if data.get('wallet_id') is None:
        return error_resp
    if data.get('currency') is None:
        return error_resp
    if data.get('amount') is None:
        return error_resp
    if data.get('transaction_id') is None:
        return error_resp

    transaction_public_id = str(uuid.uuid4())

    TRANSACTIONS[transaction_public_id] = data

    Thread(target=transaction_complete, args=(data.get('transaction_id'),)).start()

    return jsonify(dict(
        id=transaction_public_id,
        error_code=0
    ))


@app.route('/transaction/<string:transaction_public_id>', methods={'GET'})
def get_transaction_by_public_id(transaction_public_id):
    return jsonify(TRANSACTIONS[transaction_public_id])


app.run(host='0.0.0.0', port=8088, debug=False, threaded=True)
