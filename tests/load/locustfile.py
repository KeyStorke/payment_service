"""
Requirements:
  * 200 RPS on high latency laptop
"""

import uuid
import requests as r
from locust import TaskSet, HttpLocust, task

URL = '/transaction/'

BASE_ADDR = 'http://0.0.0.0'

VALID_DATA = dict(
    user_id=str(uuid.uuid4()),
    transaction_type=1,
    amount=1000,
    wallet_id=str(uuid.uuid4()),
    external_wallet_id=str(uuid.uuid4()),
    description='some text ',
    currency=0,
)

TRANSACTION_ID = r.post(f'{BASE_ADDR}{URL}', data=VALID_DATA).json().get('data').get('transaction_id')


class PaymentServiceTest(TaskSet):
    @task
    def get_transaction(self):
        self.client.get(f'{URL}{TRANSACTION_ID}')

    @task
    def get_transactions(self):
        self.client.get(f'{URL}')

    @task
    def create_transaction(self):
        self.client.post(f'{URL}', data=VALID_DATA)


class CLient(HttpLocust):
    task_set = PaymentServiceTest
    max_wait = 15000
    min_wait = 5000
