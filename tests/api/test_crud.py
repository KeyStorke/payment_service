import uuid
import pytest
import requests


URL = 'http://0.0.0.0/transaction/'

EMULATOR_URL = 'http://0.0.0.0:8088/transaction/'

SENSITIVE_DATA_FIELDS = ('user_id', 'transaction_type', 'amount', 'wallet_id', 'external_wallet_id', 'currency')

INVALID_DATA = dict(
    user_id=''.join(str(i) for i in range(1000)),
    transaction_type=500,
    amount=-10*10,
    wallet_id=''.join(str(i) for i in range(1000)),
    external_wallet_id=''.join(str(i) for i in range(1000)),
    description='some text ' * 10**4,
    currency=10**5,
)

VALID_DATA = dict(
    user_id=str(uuid.uuid4()),
    transaction_type=1,
    amount=1000,
    wallet_id=str(uuid.uuid4()),
    external_wallet_id=str(uuid.uuid4()),
    description='some text ',
    currency=0,
)


def generate_invalid_data(field):
    d = {key: VALID_DATA[key] for key in VALID_DATA if key != field}
    d[field] = INVALID_DATA[field]
    return d


def is_valid_uuid(uuid_str):
    try:
        uuid.UUID(uuid_str)
    except ValueError:
        return False
    else:
        return True


class TestCRUD:
    @property
    def transaction_id(self):
        resp = requests.post(URL, VALID_DATA)
        return resp.json().get('data').get('transaction_id')

    @property
    def transaction_private_id(self):
        transaction = self.full_transaction_data
        return transaction['transaction_id']

    @property
    def transaction_data_from_superpay(self):
        public_id = self.transaction_id
        return requests.get(f'{EMULATOR_URL}{public_id}').json()

    @property
    def full_transaction_data(self):
        public_id = self.transaction_id
        resp = requests.get(f'{URL}{public_id}')
        full_data = resp.json()
        full_data['id'] = requests.get(f'{EMULATOR_URL}{public_id}').json().get('transaction_id')
        return full_data

    @pytest.mark.parametrize(argnames='field', argvalues=SENSITIVE_DATA_FIELDS)
    def test_create_invalid_data(self, field):
        data = generate_invalid_data(field)

        # validate status code
        resp = requests.post(URL, data)
        assert resp.status_code != 200

    def test_create(self):
        resp = requests.post(URL, VALID_DATA)

        # validate status code
        assert resp.status_code == 200

        resp = resp.json()

        # validate response data
        assert resp.get('hint') == 'all correct'
        assert resp.get('code') == 0

        # validate transaction UUID
        assert is_valid_uuid(resp.get('data').get('transaction_id'))

    def test_communication_with_superpay(self):
        data = self.transaction_data_from_superpay

        assert int(data.get('amount')) == int(VALID_DATA.get('amount'))
        assert int(data.get('currency')) == int(VALID_DATA.get('currency'))
        assert str(data.get('wallet_id', '123')) == str(VALID_DATA.get('external_wallet_id'))
        assert is_valid_uuid(data.get('transaction_id'))

    def test_read_by_public_id(self):
        resp = requests.get(f'{URL}{self.transaction_id}')

        # validate status code
        assert resp.status_code == 200

        resp = resp.json()

        # validate response data
        assert is_valid_uuid(resp.get('public_id'))
        assert is_valid_uuid(resp.get('internal_wallet_id'))
        assert is_valid_uuid(resp.get('external_wallet_id'))
        assert is_valid_uuid(resp.get('user_id'))

    def test_update_by_public_id(self):
        body = {
            'status': 0
        }
        resp = requests.put(f'{URL}{self.transaction_id}', data=body)
        assert resp.status_code == 404

    def test_update_by_private_id(self):
        body = {
            'status': 0
        }

        data = self.full_transaction_data
        private_id = data['id']
        public_id = data['public_id']

        # check default status
        assert requests.get(f'{URL}{public_id}').json().get('status') == 2

        # update status
        resp = requests.put(f'{URL}{private_id}', data=body)
        assert resp.status_code == 200

        resp = resp.json()

        # validate response data
        assert resp['updated'] == 1

        # check updated data
        updated_data = requests.get(f'{URL}{public_id}').json()

        assert updated_data['public_id'] == public_id
        assert updated_data['amount'] == VALID_DATA['amount']
        assert updated_data['external_wallet_id'] == VALID_DATA['external_wallet_id']
        assert updated_data['internal_wallet_id'] == VALID_DATA['wallet_id']
        assert updated_data['type'] == VALID_DATA['transaction_type']
        assert updated_data['user_id'] == VALID_DATA['user_id']
        assert updated_data['description'] == VALID_DATA['description']
        assert updated_data['status'] == 0
        assert 'id' not in updated_data

    def test_delete_by_public_id(self):
        resp = requests.delete(f'{URL}{self.transaction_id}')
        assert resp.status_code == 405
