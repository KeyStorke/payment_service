import pytest
from django.core.exceptions import ValidationError
from apps.merchant_manager.validators import transaction_type_validator, transaction_currency_validator, \
    transaction_status_validator


class TestValidators:
    def test_transaction_status_validator(self):
        with pytest.raises(ValidationError):
            transaction_status_validator(10000)
        assert transaction_status_validator(0) is None

    def test_transaction_type_validator(self):
        with pytest.raises(ValidationError):
            transaction_type_validator(10000)
        assert transaction_type_validator(0) is None

    def test_transaction_currency_validator(self):
        with pytest.raises(ValidationError):
            transaction_currency_validator(10000)
        assert transaction_currency_validator(0) is None

