from django.db import models
from django.core.exceptions import ValidationError
from functools import wraps
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist
import logging
import uuid


def set_task(f):
    pass


class CommonInternalError(Exception):
    """ class for define internal errors """


class MethodNotAllowed(Exception):
    """ class for define exception Method not allowed"""


def get_logger(name):
    return logging.getLogger(name)


LOGGER = get_logger('merchant_manager')


class ErrorCode:
    def __init__(self, code, hint):
        self.code = int(code)
        self.hint = str(hint)


class ErrorCodes:
    NO_ERR = ErrorCode(0, 'all correct')
    INVALID_INPUT_DATA = ErrorCode(400, 'invalid input data')
    COMMON_ERROR = ErrorCode(500, 'common error')
    OBJECT_NOT_EXIST = ErrorCode(404, 'object does not exist')
    FIELD_DOES_NOT_EXIST = ErrorCode(400, 'field does not exist')
    CONFLICT_REQUEST = ErrorCode(409, 'object cant be deleted')
    METHOD_NOT_ALLOWED = ErrorCode(405, 'method not allowed')


def generate_error_resp(error):
    body = {
        'code': error.code,
        'hint': error.hint
    }
    return Response(data=body, status=error.code, content_type='application/json')


def disable(_):
    def wrapped_call(*_, **__):
        raise MethodNotAllowed()

    return wrapped_call


def handle_exceptions(f):
    @wraps(f)
    def wrapped_call(*args, **kwargs):
        try:
            call_id = uuid.uuid4().hex
            LOGGER.info('call function `{}` Call id {}'.format(f.__name__, call_id))
            result = f(*args, **kwargs)
        except (ObjectDoesNotExist, FileNotFoundError):
            LOGGER.error(
                f'call (id: {call_id}) failed with error {ErrorCodes.OBJECT_NOT_EXIST.code}')
            return generate_error_resp(ErrorCodes.OBJECT_NOT_EXIST)

        except models.FieldDoesNotExist:
            LOGGER.error(
                f'call (id: {call_id}) failed with error {ErrorCodes.FIELD_DOES_NOT_EXIST.code}')
            return generate_error_resp(ErrorCodes.FIELD_DOES_NOT_EXIST)

        except models.ProtectedError:
            LOGGER.error(
                f'call (id: {call_id}) failed with error {ErrorCodes.CONFLICT_REQUEST.code}')
            return generate_error_resp(ErrorCodes.CONFLICT_REQUEST)

        except (ValueError, AssertionError, ValidationError) as e:
            LOGGER.error(
                f'call (id: {call_id}) failed with error {ErrorCodes.INVALID_INPUT_DATA.code}: {e}')
            return generate_error_resp(ErrorCodes.INVALID_INPUT_DATA)

        except CommonInternalError:
            LOGGER.error(
                f'call (id: {call_id}) failed with error {ErrorCodes.COMMON_ERROR.code}')
            return generate_error_resp(ErrorCodes.COMMON_ERROR)

        except MethodNotAllowed:
            LOGGER.error(
                f'call (id: {call_id}) failed with error {ErrorCodes.METHOD_NOT_ALLOWED.code}')
            return generate_error_resp(ErrorCodes.METHOD_NOT_ALLOWED)

        else:
            LOGGER.info('call (id: {}) successfully completed'.format(call_id))
            return result

    return wrapped_call
