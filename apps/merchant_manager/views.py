from uuid import UUID

from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView

from .common import ErrorCodes, handle_exceptions, get_logger, disable, set_task
from .workers import payout_worker, deposit_worker
from .enums import TransactionType, TransactionStatus
from .managers import TransactionManager
from .responses import InitTransactionResult
from .serializers import TransactionSerializer
from .superpay import SuperPayApi


class TransactionsView(APIView):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.transaction_manager = TransactionManager()
        self.logger = get_logger('merchant_manager')
        self.super_pay_api = SuperPayApi(settings.SUPERPAY_BASE_ADDR)

    @handle_exceptions
    def post(self, request):
        """
        Init transaction
        POST data must contain:

        user_id: user ID
        transaction_type: type of transaction enum(PAYOUT, DEPOSIT)
        amount: how much money in transaction
        wallet_id: internal wallet ID
        external_wallet_id: external wallet ID (wallet in SuperPay for example)
        description: some description of transaction
        currency: transaction currency

        :return: error code, error hint and data. For example
         {'code':0, 'hint':'all correct', data:{'transaction_id': %transaction_id%}}

        """
        user_id = request.POST.get('user_id')
        transaction_type = request.POST.get('transaction_type')
        amount = request.POST.get('amount')
        wallet_id = request.POST.get('wallet_id')
        external_wallet_id = request.POST.get('external_wallet_id')
        description = request.POST.get('description')
        currency = request.POST.get('currency')

        input_data = (
            user_id,
            transaction_type,
            amount,
            wallet_id,
            external_wallet_id,
            description,
            currency
        )

        if None in input_data:
            data = dict(
                user_id=request.POST.get('user_id'),
                transaction_type=request.POST.get('transaction_type'),
                amount=request.POST.get('amount'),
                wallet_id=request.POST.get('wallet_id'),
                external_wallet_id=request.POST.get('external_wallet_id'),
                description=request.POST.get('description'),
                currency=request.POST.get('currency'),
            )
            self.logger.error('invalid input data: {}'.format(data))
            raise ValueError()

        assert int(amount) > 0

        transaction = self.transaction_manager.create_without_save(
            user_id=UUID(user_id),
            type=int(transaction_type),
            amount=int(amount),
            internal_wallet_id=UUID(wallet_id),
            external_wallet_id=UUID(external_wallet_id),
            description=str(description),
            currency=int(currency)
        )

        public_transaction_id = self.super_pay_api.get_public_transaction_id(
            superpay_wallet_id=UUID(external_wallet_id),
            amount=amount,
            currency=currency,
            transaction_id=transaction.id
        )

        transaction.public_id = public_transaction_id
        transaction.save()

        if transaction.type == TransactionType.PAYOUT.value:
            set_task(payout_worker)
        elif transaction.type == TransactionType.DEPOSIT.value:
            set_task(deposit_worker)
        else:
            # not supported transaction type
            pass



        resp_data = {
            'transaction_id': public_transaction_id
        }

        resp_dict = InitTransactionResult(
            code=ErrorCodes.NO_ERR.code,
            hint=ErrorCodes.NO_ERR.hint,
            data=resp_data
        ).to_dict()

        resp = Response(resp_dict, content_type='application/json')
        return resp

    @handle_exceptions
    def get(self, request):
        """
        Get all transactions
        :return: List of transactions
        """
        serialized_transactions = []

        for tr in self.transaction_manager.all():
            serialized_transactions.append(TransactionSerializer(tr).as_dict(excluded_fields=['id']))

        return Response(serialized_transactions, content_type='application/json')

    @handle_exceptions
    @disable
    def delete(self, request):
        """
        Delete all transactions

        :return: {'deleted': %how much transaction has been deleted%}
        """
        deleted = self.transaction_manager.filter().delete()[0]

        return Response({'deleted': deleted}, content_type='application/json')


class PublicTransactionView(APIView):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.transaction_manager = TransactionManager()
        self.logger = get_logger('merchant_manager')

    @handle_exceptions
    def put(self, request, transaction_public_id):
        """
        Update specified transaction

        :param transaction_public_id: transaction ID
        :return: {'updated': %how much transaction has been updated%}
        """
        if 'status' not in request.POST:
            self.logger.error(
                'invalid update (transaction_id: {}) request with POST data {}'.format(transaction_public_id,
                                                                                       request.POST))
            raise ValueError('status')
        status = TransactionStatus(int(request.POST.get('status')))
        updated = self.transaction_manager.filter(id=transaction_public_id).update(status=status.value)
        if not updated:
            raise FileNotFoundError(transaction_public_id)
        return Response({'updated': updated}, content_type='application/json')

    @handle_exceptions
    def get(self, request, transaction_public_id):
        """
        Get specified transaction

        :param transaction_public_id: transaction ID
        :return: transaction
        """
        transaction = self.transaction_manager.get(public_id=transaction_public_id)
        serialized_transaction = TransactionSerializer(transaction).as_dict(excluded_fields=['id'])
        return Response(serialized_transaction, content_type='application/json')
