from object_modeler import SlotsObjectModel, Field


class InitTransactionResult(SlotsObjectModel):
    code = Field().types(int)
    hint = Field().types(str)
    data = Field(default_value={}).types(dict)

    def __init__(self, **kwargs):
        super().__init__(kwargs)
