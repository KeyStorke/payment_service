from enum import Enum


class TransactionCurrency(Enum):
    EUR = 0
    USD = 1
    RUB = 2
    XRP = 3


class TransactionStatus(Enum):
    SUCCESS = 0
    FAILURE = 1
    IN_PROGRESS = 2
    CANCELED = 3


class TransactionType(Enum):
    PAYOUT = 0
    DEPOSIT = 1