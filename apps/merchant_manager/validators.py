from django.core.exceptions import ValidationError
from .enums import TransactionStatus, TransactionType, TransactionCurrency


def transaction_type_validator(value):
    try:
        TransactionType(int(value))
    except (ValueError, TypeError):
        raise ValidationError(f'{value} is not a TransactionType')


def transaction_currency_validator(value):
    try:
        TransactionCurrency(int(value))
    except (ValueError, TypeError):
        raise ValidationError(f'{value} is not a TransactionCurrency')


def transaction_status_validator(value):
    try:
        TransactionStatus(int(value))
    except (ValueError, TypeError):
        raise ValidationError(f'{value} is not a TransactionStatus')

