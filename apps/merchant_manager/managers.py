from django.db import models, router
from .models import Transaction
from .serializers import TransactionSerializer
from .validators import transaction_status_validator, transaction_type_validator, transaction_currency_validator


class TransactionManager(models.Manager):
    """ Transaction manager """
    def __init__(self):
        self.model = Transaction
        self._hints = {}
        self._db = router.db_for_read(self.model, **self._hints)

    def create_and_get_id(self, **kwargs):
        return self.create(**kwargs).id

    def create_without_save(self, **kwargs):
        transaction_type_validator(kwargs.get('type'))
        transaction_status_validator(kwargs.get('status', 0))
        transaction_currency_validator(kwargs.get('currency'))
        return self.model(**kwargs)

    def get_as_dict(self, **kwargs):
        transaction = self.get(**kwargs)
        return TransactionSerializer(transaction).as_dict()
