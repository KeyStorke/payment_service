import requests as r
from .common import get_logger, CommonInternalError
from uuid import uuid4


class SuperPayApi:
    def __init__(self, base_url):
        self.base_url = base_url
        self.logger = get_logger('merchant_manager')

    def get_public_transaction_id(self, superpay_wallet_id, currency, amount, transaction_id):
        payload = dict(
            wallet_id=str(superpay_wallet_id),
            currency=currency,
            amount=amount,
            transaction_id=str(transaction_id)
        )

        url = f'{self.base_url}/transaction'
        request_id = uuid4().hex

        self.logger.info(f'send request (id:{request_id}) with payload {payload} to {url}')

        try:
            resp = r.post(url=url, json=payload)

            if resp.status_code != 200:
                raise r.HTTPError(f'server returned HTTP error {resp.status_code}')

            resp = resp.json()

        except Exception as e:
            self.logger.critical(f'Exception {type(e).__name__}: {e}')
            raise CommonInternalError(e)

        if resp.get('id', '') in '':
            # TODO need error code table from SuperPay for correct handling exceptions
            error_code = resp.get('error_code')
            self.logger.error(f'request (id:{request_id}) failed with error {error_code}')
            raise ValueError(error_code)

        return resp['id']
