from django.db import models
import uuid
from .enums import TransactionType, TransactionCurrency, TransactionStatus
from .validators import transaction_type_validator, transaction_currency_validator, transaction_status_validator


class Transaction(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    public_id = models.CharField(max_length=512, default='', db_index=True)
    internal_wallet_id = models.UUIDField(verbose_name='internal wallet id')
    external_wallet_id = models.CharField(max_length=512, verbose_name='external wallet id')
    user_id = models.UUIDField(verbose_name='user id')
    amount = models.BigIntegerField(verbose_name='amount money')
    description = models.CharField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    type = models.IntegerField(choices=[(tag, tag.value) for tag in TransactionType],
                               validators=[transaction_type_validator])

    status = models.IntegerField(choices=[(tag, tag.value) for tag in TransactionStatus],
                                 default=2, validators=[transaction_status_validator])

    currency = models.IntegerField(choices=[(tag, tag.value) for tag in TransactionCurrency],
                                   validators=[transaction_currency_validator])
