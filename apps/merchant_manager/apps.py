from django.apps import AppConfig


class MerchantManagerConfig(AppConfig):
    name = 'apps.merchant_manager'
