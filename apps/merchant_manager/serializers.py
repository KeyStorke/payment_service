from rest_framework import serializers
from .models import Transaction


class TransactionSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        instance.id = validated_data.get('id', instance.id)
        instance.public_id = validated_data.get('public_id', instance.public_id)
        instance.internal_wallet_id = validated_data.get('internal_wallet_id', instance.internal_wallet_id)
        instance.external_wallet_id = validated_data.get('external_wallet_id', instance.external_wallet_id)
        instance.user_id = validated_data.get('user_id', instance.user_id)
        instance.amount = validated_data.get('amount', instance.amount)
        instance.type = validated_data.get('type', instance.type)
        instance.status = validated_data.get('status', instance.status)
        instance.currency = validated_data.get('currency', instance.currency)
        instance.description = validated_data.get('description', instance.description)
        instance.created_at = validated_data.get('created_at', instance.created_at)
        instance.last_modified = validated_data.get('last_modified', instance.last_modified)
        return instance

    def create(self, validated_data):
        return Transaction(**validated_data)

    def as_dict(self, excluded_fields=None):
        result = dict(
                id=self.instance.id,
                public_id=self.instance.public_id,
                internal_wallet_id=self.instance.internal_wallet_id,
                external_wallet_id=self.instance.external_wallet_id,
                user_id=self.instance.user_id,
                amount=self.instance.amount,
                type=self.instance.type,
                status=self.instance.status,
                currency=self.instance.currency,
                description=self.instance.description,
                created_at=self.instance.created_at,
                last_modified=self.instance.last_modified
            )

        if excluded_fields is None:
            return result
        else:
            return {field: result[field] for field in result if field not in excluded_fields}

